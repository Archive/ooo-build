#!/bin/bash

# This script helps testig OOo installation using the ooqatesttool,
# http://qa.openoffice.org/qatesttool/index.html

# IMPORTANT: This is an initial version. I plan to integrate it better with
# ooo-build, see ooo-build/doc/test-ooo.txt for more details


#TESTTOOL=/opt/openoffice.org2.0/program/testtool.bin
export testToolBin=/usr/lib/ooo3/basis3.0/program/testtool.bin

# where the qatesttool is stored (testcases)
export testToolRoot=/test/OOo/qa/qatesttool

# path to the user configuration directory
export userConfigDir=$HOME/.ooo3/

# all tests will be skipped until this script name is found
# define empty string to do not skip any test
SKIP_TO_TEST=
#SKIP_TO_TEST=writer/loadsave/w_imp_bin.bas

usage()
{
    echo "This script sta rt the qatesttool scripts from the given list"
    echo
    echo "Usage: ${0##*/} test.list [test.blacklist]"
    echo
    echo "	test.list      - list of test scripts to start"
    echo "	test.blacklist - list of test scripts to skip"
    echo 
    echo "	Both files include list of paths to the test scripts, one path"
    echo "	per line. They can also include comments prefixed by #"
    echo 
    echo "Example of the file.list:"
    echo
    echo "	# Tests for OOo-2.0.3"
    echo "	framework/first/first.bas"
    echo "	framework/first/topten.bas"
    echo "	framework/level1/f_lvl1_loadsave.bas"
}

if test -z "$1" -o "$1" = "--help" -o $# -gt 2 ; then
    usage && exit 1;
fi

if ! which dos2unix >/dev/null 2>&1 ; then
    echo "Error: dos2unix utility is not installed"
    exit 1;
fi

# list of test to be started
testList=
if  test ! -f "$1" ; then
    echo "Error: cannot read \"$1\", try --help" && exit 1;
else
    testList=`cat "$1" | dos2unix | sed "s|\#.*$||"`
fi

# list of tests to be skipped
testBlackList=
if test -n "$2" ; then
    if test ! -f "$2" ; then
	echo "Error: cannot read \"$2\", try --help" && exit 1;
    else
	testBlackList=`cat "$2" | dos2unix | sed "s|\#.*$||"`
    fi
fi
    
echo "Switching to en_US.UTF-8 locales!!!"
export LC_ALL=en_US.UTF-8

echo "Exporting OOO_FORCE_SYSALLOC=1"
export OOO_FORCE_SYSALLOC=1
echo "Exporting MALLOC_CHECK_=2"
export MALLOC_CHECK_=2

is_blacklisted()
{
    for t in $testBlackList ; do
	test "$1" = "$t" && return 0
    done
    return 1
}

run_test()
{
    test="$1"
    # first, remove the potentially broken user configuration
    rm -rf "$userConfigDir"
    "$testToolBin" -run "$test"
    sleep 5
    killall soffice.bin
    # just to be sure
    sleep 2
    killall -9 soffice.bin
}

echo "Checking userConfigDir variable..."
run_test "$testToolRoot/global/tools/closeoffice.bas"
if ! test -d "$userConfigDir" ; then
    echo "Error: The user config directory does not exists: $userConfigDir"
    echo "       Please, check the setting of the variable userConfigDir in $0"
    exit 1;
fi

# will we skip any test?
test -n "$SKIP_TO_TEST" && skip_tests=true || skip_tests=false

for test in $testList ; do
    if is_blacklisted $test ; then
	echo "Skipping blacklisted test $test..."
	continue;
    fi

    test "$test" = "$SKIP_TO_TEST" && skip_tests=false

    if $skip_tests ; then
	echo "Skipping test $test..."
    else
	echo "Starting test $test..."
	run_test "$testToolRoot/$test"
    fi
done
