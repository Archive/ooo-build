# Danish translation of openoffice strings.
# Copyright (C) 2009 Free Software Foundation, Inc.
# Ole Laursen <olau@hardworking.dk>, 2003.
# Mads Lundby <lundbymads@gmail.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: openoffice\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-05-27 23:27+0200\n"
"PO-Revision-Date: 2009-05-26 17:56+0100\n"
"Last-Translator: Mads Lundby <lundbymads@gmail.com>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../desktop/template.desktop.in.in.h:1
msgid "Office Suite"
msgstr "kontorpakke"

#: ../desktop/template.desktop.in.in.h:2
msgid "OpenOffice.org"
msgstr "OpenOffice.org"

#: ../desktop/impress.desktop.in.in.h:1
msgid ""
"Create and edit presentations for slideshows, meeting and Web pages by using "
"Impress."
msgstr ""
"Opret og redigér præsentationer til diasoplæg, møder og websider ved brug af "
"Impress."

#: ../desktop/impress.desktop.in.in.h:2
msgid "OpenOffice.org Impress"
msgstr "OpenOffice.org Impress"

#: ../desktop/impress.desktop.in.in.h:3
msgid "Presentation"
msgstr "præsentation"

#: ../desktop/base.desktop.in.in.h:1
msgid "Database"
msgstr "database"

#: ../desktop/base.desktop.in.in.h:2
msgid ""
"Manage databases, create queries and reports to track and manage your "
"information by using Base."
msgstr ""
"Administrér databaser, opret forespørgsler og rapporter til at spore og "
"administrere dine informationer ved at bruge Base."

#: ../desktop/base.desktop.in.in.h:3
msgid "OpenOffice.org Base"
msgstr "OpenOffice.org Base"

#: ../desktop/web.desktop.in.in.h:1
msgid "Create and edit Web pages by using Writer."
msgstr "Opret og redigér hjemmesider ved brug af Writer."

#: ../desktop/web.desktop.in.in.h:2
msgid "OpenOffice.org Writer/Web"
msgstr "OpenOffice.org Writer/Web"

#: ../desktop/web.desktop.in.in.h:3
msgid "Web Page Creation"
msgstr "hjemmesideredigering"

#: ../desktop/calc.desktop.in.in.h:1
msgid "OpenOffice.org Calc"
msgstr "OpenOffice.org Calc"

#: ../desktop/calc.desktop.in.in.h:2
msgid ""
"Perform calculation, analyze information and manage lists in spreadsheets by "
"using Calc."
msgstr ""
"Lav beregninger, analysér information og administrér lister i regneark ved "
"brug af Calc."

#: ../desktop/calc.desktop.in.in.h:3
msgid "Spreadsheet"
msgstr "regneark"

#: ../desktop/writer.desktop.in.in.h:1
msgid ""
"Create and edit text and graphics in letters, reports, documents and Web "
"pages by using Writer."
msgstr ""
"Opret og redigér tekst og grafik i breve, rapporter, dokumenter og "
"hjemmesider ved at bruge Writer."

#: ../desktop/writer.desktop.in.in.h:2
msgid "OpenOffice.org Writer"
msgstr "OpenOffice.org Writer"

#: ../desktop/writer.desktop.in.in.h:3
msgid "Word Processor"
msgstr "tekstbehandling"

#: ../desktop/draw.desktop.in.in.h:1
msgid "Create and edit drawings, flow charts, and logos by using Draw."
msgstr "Opret og redigér tegninger, rutediagrammer og logoer ved brug af Draw."

#: ../desktop/draw.desktop.in.in.h:2
msgid "Drawing"
msgstr "tegneprogram"

#: ../desktop/draw.desktop.in.in.h:3
msgid "OpenOffice.org Draw"
msgstr "OpenOffice.org Draw"

#: ../desktop/math.desktop.in.in.h:1
msgid "Create and edit scientific formulas and equations by using Math."
msgstr "Opret og redigér videnskabelige formler og ligninger ved brug af Math."

#: ../desktop/math.desktop.in.in.h:2
msgid "Formula"
msgstr "formel"

#: ../desktop/math.desktop.in.in.h:3
msgid "OpenOffice.org Math"
msgstr "OpenOffice.org Math"

#~ msgid "Drawing (1.1)"
#~ msgstr "Tegneprogram (1.1)"

#~ msgid "OpenOffice.org drawing package"
#~ msgstr "OpenOffice.org-tegnepakke"

#~ msgid "OpenOffice.org presentation application"
#~ msgstr "OpenOffice.org-præsentationsprogram"

#~ msgid "Presentation (1.1)"
#~ msgstr "Præsentation (1.1)"

#~ msgid "Spreadsheet (1.1)"
#~ msgstr "Regneark (1.1)"

#~ msgid "OpenOffice.org word processor"
#~ msgstr "OpenOffice.org-tekstbehandler"

#~ msgid "Word Processor (1.1)"
#~ msgstr "Tekstbehandler (1.1)"
