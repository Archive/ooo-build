# Czech translation of ooo-build.
# Copyright (C) 2003, 2005, 2006 ooo-build'S COPYRIGHT HOLDER
# Copyright (C) 2005, 2006 Miloslav Trmac <mitr@volny.cz>
# This file is distributed under the same license as the ooo-build package.
# Miloslav Trmac <mitr@volny.cz>, 2003, 2005 - 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: ooo-build VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-02-09 21:26+0100\n"
"PO-Revision-Date: 2006-02-13 05:50+0100\n"
"Last-Translator: Miloslav Trmac <mitr@volny.cz>\n"
"Language-Team: Czech <cs@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../desktop/template.desktop.in.in.h:1
msgid "Office Suite"
msgstr "Kancelář"

#: ../desktop/template.desktop.in.in.h:2
msgid "OpenOffice.org"
msgstr "OpenOffice.org"

#: ../desktop/impress.desktop.in.in.h:1
msgid ""
"Create and edit presentations for slideshows, meeting and Web pages by using "
"Impress."
msgstr ""
"Vytvářejte a upravujte prezentace pro ukázky, porady a webové stránky pomocí "
"Impressu."

#: ../desktop/impress.desktop.in.in.h:2
msgid "OpenOffice.org Impress"
msgstr "OpenOffice.org Impress"

#: ../desktop/impress.desktop.in.in.h:3
msgid "Presentation"
msgstr "Prezentace"

#: ../desktop/base.desktop.in.in.h:1
msgid "Database"
msgstr "Databáze"

#: ../desktop/base.desktop.in.in.h:2
msgid ""
"Manage databases, create queries and reports to track and manage your "
"information by using Base."
msgstr ""
"Spravujte databáze, vytvářejte dotazy a sestavy pro sledování a řízení "
"vašich informací pomocí Base."

#: ../desktop/base.desktop.in.in.h:3
msgid "OpenOffice.org Base"
msgstr "OpenOffice.org Base"

#: ../desktop/web.desktop.in.in.h:1
msgid "Create and edit Web pages by using Writer."
msgstr "Vytvářejte a upravujte WWW stránky pomocí Writer."

#: ../desktop/web.desktop.in.in.h:2
msgid "OpenOffice.org Writer/Web"
msgstr "OpenOffice.org Writer/WWW"

#: ../desktop/web.desktop.in.in.h:3
msgid "Web Page Creation"
msgstr "Tvorba WWW stránek"

#: ../desktop/calc.desktop.in.in.h:1
msgid "OpenOffice.org Calc"
msgstr "OpenOffice.org Calc"

#: ../desktop/calc.desktop.in.in.h:2
msgid ""
"Perform calculation, analyze information and manage lists in spreadsheets by "
"using Calc."
msgstr ""
"Provádějte výpočty, analyzujte informace a ovládejte seznamy v sešitech "
"pomocí Calcu."

#: ../desktop/calc.desktop.in.in.h:3
msgid "Spreadsheet"
msgstr "Tabulkový kalkulátor"

#: ../desktop/writer.desktop.in.in.h:1
msgid ""
"Create and edit text and graphics in letters, reports, documents and Web "
"pages by using Writer."
msgstr ""
"Vytvářejte a upravujte text a obrázky v dopisech, sestavách, dokumentech a "
"webových stránkách pomocí Writeru."

#: ../desktop/writer.desktop.in.in.h:2
msgid "OpenOffice.org Writer"
msgstr "OpenOffice.org Writer"

#: ../desktop/writer.desktop.in.in.h:3
msgid "Word Processor"
msgstr "Textový procesor"

#: ../desktop/draw.desktop.in.in.h:1
msgid "Create and edit drawings, flow charts, and logos by using Draw."
msgstr "Vytvářejte a upravujte kresby, bloková schémata a loga pomocí Draw."

#: ../desktop/draw.desktop.in.in.h:2
msgid "Drawing"
msgstr "Kreslení"

#: ../desktop/draw.desktop.in.in.h:3
msgid "OpenOffice.org Draw"
msgstr "OpenOffice.org Draw"

#: ../desktop/math.desktop.in.in.h:1
msgid "Create and edit scientific formulas and equations by using Math."
msgstr "Vytvářejte a upravujte vědecké výpočty a vzorce pomocí Math."

#: ../desktop/math.desktop.in.in.h:2
msgid "Formula"
msgstr "Vzorec"

#: ../desktop/math.desktop.in.in.h:3
msgid "OpenOffice.org Math"
msgstr "OpenOffice.org Math"
