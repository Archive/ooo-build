# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ooo-build\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-01-20 10:33+0200\n"
"PO-Revision-Date: 2006-01-23 14:46+0100\n"
"Last-Translator: <openoffice@lists.ximian.com>\n"
"Language-Team: <openoffice@lists.ximian.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: desktop/base.desktop.in.in:16
msgid "Manage databases, create queries and reports to track and manage your information by using Base."
msgstr "គ្រប់គ្រង​មូលដ្ឋាន​ទិន្នន័យ បង្កើត​សំណួរ និង រាយការណ៍​ ដើម្បី​តាមដាន និង គ្រប់គ្រង​ព័ត៌មាន​របស់​អ្នក​ដោយ​ប្រើ Base ។"

#: desktop/calc.desktop.in.in:15
msgid "Perform calculation, analyze information and manage lists in spreadsheets by using Calc."
msgstr "គណនា​ វិភាគ​ព័ត៌មាន និង គ្រប់គ្រង​បញ្ជី​ក្នុង​សៀវភៅ​បញ្ជី ដោយ​ប្រើ Calc ។"

#: desktop/draw.desktop.in.in:16
msgid "Create and edit drawings, flow charts, and logos by using Draw."
msgstr "បង្កើត និង កែ​សម្រួល​គំនូរ គំនូស​តាង​លំហូរ និង រូបសញ្ញា​ដោយ​ប្រើ Draw ។"

#: desktop/impress.desktop.in.in:16
msgid "Create and edit presentations for slideshows, meeting and Web pages by using Impress."
msgstr "បង្កើត និង កែ​សម្រួល​ការ​បង្ហាញ​សម្រាប់​បង្ហាញ​ស្លាយ កិច្ច​ប្រជុំ និង ទំព័រ​បណ្តាញ​ដោយ​ប្រើ Impress ។"

#: desktop/writer.desktop.in.in:16
msgid "Create and edit text and graphics in letters, reports, documents and Web pages by using Writer."
msgstr "បង្កើត និង កែ​សម្រួល​អត្ថបទ និង ក្រាហ្វិក​ក្នុង​សំបុត្រ របាយការណ៍ ឯកសារ និង ទំព័រ​បណ្តាញ​ដោយ​ប្រើ Writer ។"

#: desktop/math.desktop.in.in:15
msgid "Create and edit scientific formulas and equations by using Math."
msgstr "បង្កើត និង កែ​សម្រួល​រូបមន្ត​វិទ្យាសាស្ត្រ និង សមីការ​ដោយ​ប្រើ Math ។"
