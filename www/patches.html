<html>
<body>
<h1>Some brief notes on patch submission</h1>

<h3>Contents</h3>

<ul>
    <li><a href="#read-it">Read your patch</a></li>
    <li><a href="#cleanliness">Cleanliness</a></li>
    <li><a href="#style">Coding Style</a></li>
    <li><a href="#copy-n-paste">Copy &amp; Paste</a></li>
    <li><a href="#tab-stops">Tab stops</a></li>
    <li><a href="#things-to-test">Testing</a></li>
    <li><a href="#explanation">Explanation</a></li>
</ul>

<p>
  The aim of this chunk of text is to encourage people to produce
beautiful, consistent, maintainable code (as patches) that one
can take pleasure in reading.
</p>

<h3 id="read-it">Read your patch</h3>
<p>
  You should read what the reviewer is going to read; ie. when you
take your <code>diff -u &lt;files&gt;</code>, read it through
rather than just mailing it. If you see chunks like this:
</p>
<pre>@@ -838,9 +838,24 @@
...
	rtl::OUString   aWorkPath;
-
+   
 	aSecurity.getHomeDir( aHomePath );
...</pre>
<p>
  Then it's best to manually chop these out of the patch - this
is just some rather irrelevant whitespace / other change that
has happened. Strip all your debugging code out, and ensure what
remains makes sense.
</p>
<p>
  Reading the patch also has the distinct advantage of refreshing
your memory of the whole change, which often flags missing pieces.
If you notice any, go back and make it complete, then re-diff.
</p>
<p>
  Some people believe that line-count is important in patches -
however, the opposite is true, particularly for fixes; a neat &amp;
well contained solution is usually best.
</p>

<h3 id="cleanliness">Cleanliness: code hygine</h3>
<h5 id="commented-cruft">Commented out cruft</h5>
<p>
  Leaving commented out code lying around is really bad news. It
also advertises the programmers' uncertainty to the reader; such
an uncertain change should never be committed. There is no point
in pretending we have a fix, unless we are certain it is the correct
fix. Until we have exceeded the understanding of the original
author who made the slip, we can't make a reliable &amp; accurate
fix. A simple example:
</p>
<p>
<pre>//    if( 0 != a + 1 ) removed - looks wrong
    if( 0 != a )</pre>
Of course - as is likely with carelessly thought out code, that
isn't written with a full understanding, this is likely to be
wrong again; so we get:
<pre>//    if( 0 != a + 1 ) removed before later H.Opeless change
//    if( 0 != a ) - removed H.Opeless 2005/01/26 bug #123456
    if( 0 != a - 1 )</pre>
Notice how the programmer has helpfully tried to improve his lot
by referencing the bug he (hopefully) fixed. Notice how unconfident
he is in his fix. If this is indeed the correct fix. Notice also
how the date information is redundant &amp unreliable clutter
source should always be verified from an RCS for regression analysis.
</p>
<p>
  Poor programmers often 'try' things, ie. when they reach a
problem, instead of reading around it, understanding it well and
then making a simple incision; they start fiddling (unsystematically)
with ever increasing combinations of tweaks, testing each one -
without any real understanding. Luckily they often leave a great
trail of commented out things they tried before (perhaps)
happening on a fix.
</p>
<p>
An <i>excellent</i> programmer would provide a patch that removed
any such cruft; it would perhaps add a single line and remove 3.
<pre>-//    if( 0 != a + 1 ) removed before H.Opeless change
-//    if( 0 != a ) - removed H.Opeless 2005/01/26 bug #123456
-    if( 0 != a - 1 )
+    if( a > 1 )</pre>
It would require no comment. It would perhaps also fix a
similar mis-understanding in 2 other places; and add some
pre-conditions or assertions, such that this couldn't happen
again. She would also perhaps re-write a few German comments &amp;
rename a few German variable-names to English equivalents. She
would also provide a simple justification to the patch reviewer
to make thei job easier.
</p>
<h5 id="commenting-fetish">Over-commenting</h5>
<p>
  No over-commenting: comments may describe the function of
functions - however, functions should have clear names to avoid
the necessity for that; people doing:
<code>if( curLang & LANGUAGE_ENGLISH == LANGUAGE_ENGLISH )	// Some sort of english language</code>
style commenting will be laughed at &amp; told to remove it.
</p>
<h5 id="sensible-bracketing">Bracket over-use</h5>
<p>
  Some people like (to (insert(random) ) brackets) in their
expressions that server no useful purpose. Of course - sometimes
brackets are helpful - when operator precedence is not obvious;
eg. <code>a = 1 << 2 + 3</code> is a shooting offence [ NB. this
is suprisingly effectively <code>a = 1 << (2 + 3)</code> ].
</p>
<p>
  Other places people like to abuse brackets: 'return' is not a
function: ie. <code>return 3;</code> not <code>return (3);</code>
</p>

<h3 id="style">Style</h3>
<p>
  Carefully written code matches the style of the surrounding
code, or - where that is hetrogenous - creates an oasis of clean
homogenous style amid the mess. Your code should be consistent
and conservative wrt. whitespace usage.
</p>
<p>
  It's well worth reading Linus' <a
href="http://www.adamspiers.org/computing/Linus-Kernel-CodingStyle">
coding style</a> document - although ignoring the tab-stop section.
Particularly wasting vspace is frowned upon.
<pre>if( something )
{
    one_line();
}
else
{
    another_line();
}</pre>
Wastes huge chunks of vspace; it should be:
<pre>if( something )
    one_line();
else
    another_line();</pre>
But never:
<pre>if( something) one_line();
else another_line();</pre>
or something equally cramped.
</p>

<h3 id="copy-n-paste">Copy &amp; Paste</h3>
<p>
  In the bad old days before keyboards, cutting and pasting
took lots of time, glue &amp; co-ordination; it was also obvious
that it filled your 128 bytes of memory with duplicate rubbish.
In the modern world - the invention of the copy/paste combination
has lead to a massive increasing in programmer productivity. Now -
without the use of artificial arms, thousands of lines of bad code
can be duplicated in many different places all over a largeish
project. An added career benefit is that it looks as if you are
<b>really</b> clever to have (apparently) written such large
chunks of (what looks like) new code so quickly.
</p>
<p>
  <b>Conversely</b> - anyone cutting and pasting more than 2 lines of code
will just be laughed at, called an amateur, and poked with a sharp
stick; before sending them to tame some vicious, but simple sounding
bug somewhere else. The net result of a nice patch should be to
reduce complexity &amp; code-size while adding features and increasing
maintainability; if some existing code needs to be re-factored to
avoid copy/paste - then you need to do that first; it's not optional.
Particularly laughed at are large blocks of very similar code with a
few numbers changed.
</p>

<h3 id="tab-stops">Tab stops</h3>
<p>
    So - this is an old problem, and one that keeps coming back
to bite people. Here is the problem: a tab character is 0x09.
A space is 0x20. Indentation - is determined by the amount of
visible space before code on a given line.
</p>
<p>
    When an editor / viewer renders a '<i>0x09</i>' character there is
a disagreement as to how many on-screen '<i>0x20</i>' characters to
transliterate it into. Often in Unix-land it's 8. In Windows-land
it's often 4 (by default etc.). ie. "<i>0x09</i>Hello World" would be:
<pre>    Hello World	        on Win32
        Hello World     in Linux</pre>
<p>
So what you say; it should be at least consistent. The problem is - for
smaller indents there is a consistent method - the space. ie. for a small
indents of 4 stops on Unix - you can just use 4 spaces; leaving tabs for
the bigger 8 stop indents. ie.
<pre>{
    if (foo)
<i>0x09</i>baa();
}</pre>
<p>Which will look fine on default Unix:</p>
<pre>{
    if (foo)
        baa();
}</pre>
<p>But on Win32 since the 0x09 will turn into 4 spaces; you will see:</p>
<pre>{
    if (foo)
    baa();
}</pre>
<p>which is ugly - and of course, is only a simple instance of this problem.</p>
<p>
How to fix it - since the OO.o standard is different to the stock Unix
standard - first we need to configure our editor to deal with spaces, inside
emacs you can use <code>M-x set-variable tab-width 4</code> to change the view
of a single file; or a <code>setq tab-width 4</code> hook for certain files.
</p>
<p>
When it all goes wrong: there is an app called 'expand' that will convert all
your <i>0x09</i> tab characters to a given number of spaces. If you created
the <b>entire</b> file on Unix ie. it <i>all</i> has this 8stop tab assumption,
then <code>expand &lt;file&gt;</code> will do what you want.

<h3 id="things-to-test">Testing</h3>
<p>
    There are a few things that people tend to forget about when altering
the code - that require special care and testing.
    <ul>
	<li>
	  <b>Redo/Undo</b> - if you're changing a UI relevant feature, have you
	  added code with the correct *do contexts ? when you use this
	  operation, does undo work correctly, how about a subsequent redo ?
	</li>
	<li>
	  <b>Load/Save</b> - if you added a feature - does it serialize
	  correctly, when you save some data &amp; close the file, can you
	  load it again ? Is it backwards compatible with the older XML file
	  format ?
	</li>
	<li>
	  <b>Scripting</b> - lots of code has special cases for scripting,
	  where many invariants that are true in GUI use are no longer true,
	  in particular various interaction related objects may not be
	  present; it's important to do something sensible for this case;
	  or at least consider what defensive coding against this is used
	  in similar code in that context.
	</li>
	<li>
	  <b>I18n</b> - will it continue to work in other languages - or
	  have you re-ordered things so as to break them ? eg. never prepend
	  items to a stringlist used in a combo-box in only 1 language;
	  indeed never pre-pending anything to an existing enumeration /
	  item list is prolly a good rule of life. Test it in a new language.
	</li>
    </ul>
</p>

<h3 id="explanation">Explain your patch</h3>
<p>
  The person that reviews your patch is going to need to understand the
context, and your reasoning. Hopefully the code you're changing is clear,
and the bug is quite obvious, in that case - no explanation is necessary.
Perhaps it's a simple kind of bug: eg. a buffer over-run. Then something
like:
</p>
<p>
<pre>-      for( i = 0; i < nLength; i++ )
+      for( i = 0; i < nLength + 1; i++ )</pre>
<i>nLength is not the real length, cf. it's decl.</i>would be adequate.
</p>
<p>
  When the bug or patch is more complex, proportionally more explanation
is required. No essays are required though - bad grammer, mis-spelliing,
rough code pointers &amp; terse explanation are fine. If you find the
reviwer is asking you for an ever more detailed analysis - it's most
likely because they think your fix is wrong &amp; want to encourage
you to find that our for yourself - try thinking again - of course,
they in turn may be wrong - convince them.
</p>
<p>
When the patch re-factors, or re-indents a chunk of code, you
can get hundreds of lines of unreadable diff; it's nice to have a comment
on any substantive changes there: <i>Re-factored getFoo() into two methods
getBaa() and getBaz() to allow re-use in getNurgh() - no other
changes</i> is great.
</p>

</body>
</html>